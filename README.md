Seven Oaks Townhomes — The best townhome for an apartment price! Seven Oaks Townhomes is now under new ownership and new management! Our unit interiors are undergoing major upgrades, we’re adding amenities, installing new windows and increased staffing. Come experience the difference today!

Address: 200 Seven Oaks Road, Durham, NC 27704

Phone: 919-471-6493